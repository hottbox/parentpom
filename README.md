# Deployment-Merker

Da das Artefakt `buildTools-maven-plugin` die `parentPom` verwendet
und diese wiederum `buildTools-maven-plugin`, entsteht beim Build ein "Henne-Ei-Problem".

Wenn in der `parentPom` bereits die zu installierende/deployende Version von `buildTools-maven-plugin` referenziert wird,
so ist folgendermaßen vorzugehen:

1) Finale Stände mit SNAPSHOT-Versionen in Git einchecken für Projekte: `parentPom`, `buildTools-maven-plugin`

2) Versionen auf Release-Version ändern für Projekte: `parentPom`, `buildTools-maven-plugin` (**nur temporär & lokal!**)

3) Installation der `parentPom` ins _localRepository_: `mvn install`

4) Installation von `buildTools-maven-plugin` ins _localRepository_: `mvn install`

5) lokale Versionsänderungen rückgängig machen

6) Hottbox-Build-Pipeline ausführen: `runBuild.sh` > RELEASE-Build > Projekt/Branch > ...

   - zuerst für `parentPom`,
   - danach für `buildTools-maven-plugin`
