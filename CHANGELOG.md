## Changelogs / Release notes

All notable changes to the project `de.horstgernhardt:parenrPom` are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---

#### [Unreleased/CurrentImplementation/NextRelease]

##### Added

- 

##### Changed

- 

##### Deprecated

- 

##### Removed

- 

##### Fixed

- 

##### Security

- 

##### Known issues

- 
