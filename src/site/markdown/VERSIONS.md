## Maven sites for versions/version patterns

If you need the project's documentation (Maven site) for a concrete version – even if its version pattern isn't listed here –
just check the maven repository for the appropriate site-jar.

For each of the following version patterns the corresponding latest Maven site is available online:

- [LATEST](../)
- [1.0.x](../1.0.x/)

