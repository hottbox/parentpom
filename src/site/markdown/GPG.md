## Artifact verification

If you want/need to verify this porject's artifacts, you can use [GPG (GnuPG)](http://www.gnupg.org/);
it's a freely available implementation of the OpenPGP standard.

1) Download the desired artifact and it's signature file (`*.asc`);  
   at least in a Maven environemnet these files are normally already present in your local repository, if you define the artifact as dependency.

2) Import the required key with ID `36248417F4F8A50F1720AB6B5E427335E1C523E6`:  
   `gpg --keyserver keyserver.ubuntu.com --recv-keys 36248417F4F8A50F1720AB6B5E427335E1C523E6`  
   You can use the following values as `--keyserver` argument:
   - `keyserver.ubuntu.com`
   - `keys.openpgp.org`
   - `pgp.mit.edu`

3) Verify the artifact:  
   `gpg --verify parentPom-1.0.0.pom.asc parentPom-1.0.0.pom`  
   The last argument
   - is optional, if the signature file has the same name plus `.asc`
   - is required, if the signature file has a complete different name.
