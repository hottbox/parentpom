## Changelogs / Release notes

All notable changes to the project `de.horstgernhardt:parentPom` are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---

#### [1.0.0] - 2023-05-25
Project creation
